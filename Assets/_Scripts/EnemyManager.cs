﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public float speed;
    public PlayerManager playerRef;
    public bool isStuned = false;
    public float sTime = 0;
    private float sAux;
    private RaycastHit hit;


	// Use this for initialization
	void Start ()
    {
		sAux = sTime;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(isStuned == false)
        {
            transform.Translate(0, 0, speed * Time.deltaTime);
        }
        else
        {
            sTime--;
            if (sTime <= 0)
            {
                Debug.Log("entrou!");
                isStuned = false;
                sTime = sAux;
            }

            
        }   
   
	}

    private void FixedUpdate()
    {
        if(isStuned == false)
        {
            EnemyView();
        }
        
    }

    private void ColliderManager(string tipo)
    {
        switch (tipo)
        {
            case "right":
                this.transform.rotation = Quaternion.Euler(0, 90, 0);

                break;
            case "left":
                this.transform.rotation = Quaternion.Euler(0, 270, 0);
                break;
            case "down":
                this.transform.rotation = Quaternion.Euler(0, 180, 0);
                break;
            case "up":
                this.transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
        }
    }

    private void EnemyView()
    {
        //vector3 da posição da frente do objeto
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, fwd, out hit))
        {
            if (hit.collider.CompareTag("Player"))
            {
                //Mensagem no console confirmando q tem um objeto na frente
                Debug.Log("achou o fodido!");
                playerRef.haveSeen = true;
                speed = 0;
            }
            
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("nasceu caralho!");
        if (collision.gameObject.tag.Equals("obstacle"))
        {
            ColliderManager(collision.gameObject.name);
            
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            playerRef.haveSeen = true;
            speed = 0;
        }
    }

}
