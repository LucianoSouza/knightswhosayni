﻿//Criado em 07/04 por luciano souza
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerManager : MonoBehaviour
{
    public float speed;
    public bool haveKey = false;
    public bool haveSeen = false;
    public EnemyManager enemyRef;
    private int idLado = 0;
    private string sceneName;
    private RaycastHit hit;
   // Rigidbody rigidbody;


    // Use this for initialization
    void Start()
    {
        sceneName = SceneManager.GetActiveScene().name;
    }

    // Update is called once per frame
    void Update()
    {
        if (haveSeen == false)
        {
            //função de movimentação
            PlayerMoviment(speed);
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(sceneName);
            }
        }
        
        
    }

    private void FixedUpdate()
    {
        if (haveSeen == false)
        {
            //chamada da função de teste do raycast
            DetectionTest();
        }
        
    }


    //Função de teste de raycast
    private void DetectionTest()
    {
        //vector3 da posição da frente do objeto
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        //verifica o input do jogador
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Usa o raycast para verificar se tem algo na frente do objeto
            if (Physics.Raycast(transform.position, fwd, out hit))
            {
                if (hit.collider.CompareTag("Enemy"))
                {
                    enemyRef.isStuned = true;
                }
                else
                {
                    enemyRef.isStuned = false;
                }
                //Mensagem no console confirmando q tem um objeto na frente
                Debug.Log("S");
            }
            else
            {
                //Mensagem no console negando q tem um objeto na frente
                Debug.Log("N");
            }
        }
    }
    
    //função que rotaciona o player de acordo com a frente
   public void PlayerRotarion(int lado)
    {
        switch (lado)
        {
            case 0:
                transform.rotation = Quaternion.Euler(0, 180, 0);
                break;
            case 1:
                transform.rotation = Quaternion.Euler(0, 90, 0);
                break;
            case 2:
                transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case 3:
                transform.rotation = Quaternion.Euler(0, 270, 0);
                break;
        }
    }

    //função de input e movimentação do player
    public void PlayerMoviment(float s)
    {
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            idLado = 0;
            PlayerRotarion(idLado);
            //rigidbody.AddForce(0, 2, 0);
            transform.Translate(0, 0, s * Time.deltaTime);
        }

        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            idLado = 1;
            PlayerRotarion(idLado);
            //rigidbody.AddForce(0, 2, 0);
            transform.Translate(0, 0, s * Time.deltaTime);
        }

        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            idLado = 2;
            PlayerRotarion(idLado);
           // rigidbody.AddForce(0, 2, 0);
            transform.Translate(0, 0, s * Time.deltaTime);
        }

        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            idLado = 3;
            PlayerRotarion(idLado);
           // rigidbody.AddForce(0, 2, 0);
            transform.Translate(0, 0, s * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("key"))
        {
            haveKey = true;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.name.Equals("door") && haveKey == true)
        {
            SceneManager.LoadScene(1);
        }
    }




    /*
    private void PlayerAnimSimple(float f,float s)
    {
        rigidbody.AddForce(0, f, 0);
        transform.Translate(0, 0, speed * Time.deltaTime);
        
    }
    */
}
