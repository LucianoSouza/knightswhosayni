﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {

    public Text keyHud;
    public Text restartHud;
    public PlayerManager player;
    // Use this for initialization
    void Start ()
    {
        keyHud = GameObject.Find("Key").GetComponent<Text>();
        restartHud = GameObject.Find("Game over").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(player.haveKey == true)
        {
            keyHud.text = "KEY FOUND";
        }

        if(player.haveSeen == true)
        {
            restartHud.text = "Press R to restart";
        }
	}
}
