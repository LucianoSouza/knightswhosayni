﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuHUDManeger : MonoBehaviour
{
    public GameObject botao1, botao2, botao3;

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {

    }

    public void SceneChanger()
    {
        SceneManager.LoadScene(1);
    }

    public void FaseSelection()
    {
        SceneManager.LoadScene(4);
    }
    
    public void BotaoFase1()
    {
        SceneManager.LoadScene(1);
    }
    public void BotaoFase2()
    {
        SceneManager.LoadScene(2);
    }

    public void BotaoFase3()
    {
        SceneManager.LoadScene(3);
    }

    public void BotaoVoltar()
    {
        SceneManager.LoadScene(0);
    }
}
